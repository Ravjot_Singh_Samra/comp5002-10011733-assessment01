﻿using System;

namespace comp5002_10011733_assessment01
{
    class Program
    {
        static void Main(string[] args)
        {
            // <PSEUDO CODE> || <CODE DOCUMENTATION>
            var name = (String) null; // Declare variable "name" string null || null for empty
            var decision = (String) null; // Declare variable "decision" string null || used string instead of boolean
            var userNum = (Double) 0; // Declare variable "userNum" double 0 || 0 for empty
            var userNum2 = (Double) 0; // Declare variable "userNum2" double 0
            var GST = (Double) 1.15; // Declare variable "GST" double 1.15 || 1.15 for 15%, double instead of int because GST can have decimal places, variable so it's easier to update GST

            Console.WriteLine("\n=============================================================="); // || formatting for console application, \n for next line and easier to read, === for visual design
            Console.WriteLine("==============================================================");
            Console.WriteLine("==================== WELCOME TO OUR SHOP! ===================="); // Output: print welcome to shop message
            Console.WriteLine("==============================================================");
            Console.WriteLine("==============================================================\n");
            Console.WriteLine("- Please enter your full name.\n"); // Output: print question for variable "name"

            name = Console.ReadLine(); // Input: store: variable "name"

            Console.WriteLine($"\n- Hello, {name}."); // Output: print greeting with variable "name" string value
            Console.WriteLine("- Please only enter the full cost of your product (including all cent values/decimal places).\n"); // Output: print request for variable "userNum" 

            userNum = Double.Parse(Console.ReadLine()); // Input: store: variable "userNum" double
                
            Retry: // label || for the single goto statement

            Console.WriteLine("\n- Would you like to add another price?\n"); // Output: print request decision for variable "userNum2"

            decision = Console.ReadLine(); // Input: store: variable "decision" string
            {
                if (decision.ToLower() == "yes" || decision.ToLower() == "y" || decision.ToLower() == "true") // if: decision value is checked true || ToLower() makes the user input lowercase so no need to do numerous decision == "yES" and decision == "YeS" etc.
                {
                        Console.WriteLine($"\n- Enter another price with the full cost. (Current Cost: ${ userNum } NZD)\n"); // Output: print request for variable "userNum2" with variable "userNum" string value, currency unit

                        userNum2 = Double.Parse(Console.ReadLine()); // Input: store: variable "userNum2" double
                        
                        Console.WriteLine($"\n- Final Total Cost EXCL. GST: ${ userNum + userNum2 } NZD."); // Output: print userNum+userNum2, currency unit, notify GST exclusive
                }
                
                else if (decision.ToLower() == "no" || decision.ToLower() == "n" || decision.ToLower() == "false") // else if: decision value is checked false 
                {
                        Console.WriteLine($"\n- Final Total Cost EXCL. GST: ${ userNum } NZD."); // Output: print userNum, currency unit, notify GST exclusive
                }
                
                else // else
                {
                        Console.WriteLine("- Unacceptable input. Only answer yes or no, try again.\n\n"); // Output: print error and give feedback on how to do proper user input
                        goto Retry; // goto label: retry asking user input for decision
                }
            }

            Console.WriteLine($"- Final Total Cost INCL. GST: ${ Math.Round((( userNum + userNum2 ) * GST), 2) } NZD.\n"); // Output: print userNum+userNum2 then multiple by GST variable, currency unit, notify GST inclusive || Math.Round used because money in 4 significant figures looks tidy/normal, userNum2 is 0 and does nothing in the addition if decision was false
            Console.WriteLine($"- Thank you for your purchase, {name}. Press any key to exit. Goodbye!\n"); // Output: print thank you and goodbye message with variable "name" string value, notify press any key to end 
        
            Console.ReadKey(); // Input: key press: program end || Needed for running program directly from executable instead of terminal or powershell so it doesn't terminate before user can see and realize final costs
        }
    }
}
